module.exports = function(Sequelize, sequelize, Establishment, EnvironmentItem) {
  class Environment extends Sequelize.Model {}
  Environment.init({
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1
    },
    ownerUser: Sequelize.STRING,
    inspectUser: Sequelize.STRING,
    status: {
      type: Sequelize.ENUM,
      values: ["enable", "disable"]
    },
    name: Sequelize.STRING,
    ower: Sequelize.STRING,
    description: Sequelize.STRING,
    dateStatus: Sequelize.DATE,
    envcategoryId: Sequelize.STRING,

    itens: {
      type: Sequelize.TEXT('long'),
      get: function() {
        try {
          var json = JSON.parse(this.getDataValue("itens"));
          console.log(json);
          return json;
        }
        catch (e) {
          console.log(e);
        }
      },
      set: function(val) {
        try {
          return this.setDataValue("itens", JSON.stringify(val));
        }
        catch (e) {}
      }
    },
  }, { sequelize, modelName: "environment" });


  Environment.hasOne(Establishment);


  return Environment;
};
