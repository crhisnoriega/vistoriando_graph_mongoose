module.exports = function(Sequelize, sequelize) {
  class Surver extends Sequelize.Model {}
  Surver.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: Sequelize.STRING,
      status: {
        type: Sequelize.ENUM,
        values: [
          "new",
          "waiting_surveryor",
          "surveryor_taked",
          "cancel",
          "finisehd",
          "outro"
        ]
      },

      tipo_servico: Sequelize.STRING,
      modelo_vistoria: Sequelize.STRING,
      contrato: Sequelize.STRING,
      data: Sequelize.STRING,
      hora: Sequelize.STRING,
      observacap: Sequelize.STRING,

      agency: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("agency"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("agency", JSON.stringify(val));
          } catch (e) {}
        }
      },

      imovel: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("imovel"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("imovel", JSON.stringify(val));
          } catch (e) {}
        }
      },

      vistoriador: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("vistoriador"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("vistoriador", JSON.stringify(val));
          } catch (e) {}
        }
      },

      locatarios: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("locatarios"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("locatarios", JSON.stringify(val));
          } catch (e) {}
        }
      },

      locadores: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("locadores"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("locadores", JSON.stringify(val));
          } catch (e) {}
        }
      }
    },
    { sequelize, modelName: "surver" }
  );

  return Surver;
};
