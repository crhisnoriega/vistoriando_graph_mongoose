module.exports = function(Sequelize, sequelize) {
  class Medidor extends Sequelize.Model {}
  Medidor.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: Sequelize.STRING,
      status: {
        type: Sequelize.ENUM,
        values: ["enable", "disable"]
      },
      unit: Sequelize.STRING,
      other: Sequelize.STRING
    },
    { sequelize, modelName: "medidor" }
  );

  return Medidor;
};
