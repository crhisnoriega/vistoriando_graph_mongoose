module.exports = function(Sequelize, sequelize, Image) {
  class InspectionItem extends Sequelize.Model {}
  InspectionItem.init({
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1
    },
    environmentId: Sequelize.STRING,
    description: Sequelize.STRING,
    imageId: Sequelize.STRING,
    status: {
      type: Sequelize.ENUM,
      values: ["ok", "bad", "unknow"]
    }
  }, { sequelize, modelName: "inspectionitem" });

  InspectionItem.hasMany(Image);

  return  InspectionItem;
};
