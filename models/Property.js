module.exports = function(Sequelize, sequelize) {
  class Property extends Sequelize.Model {}
  Property.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: Sequelize.STRING,

      cep: Sequelize.STRING,
      street: Sequelize.STRING,
      number: Sequelize.STRING,
      complemento: Sequelize.STRING,
      bairro: Sequelize.STRING,
      city: Sequelize.STRING,
      state: Sequelize.STRING,
      email: Sequelize.STRING,

      superAgency: Sequelize.STRING,
      accountId: Sequelize.STRING
    },
    { sequelize, modelName: "property" }
  );

  return Property;
};
