module.exports = function(Sequelize, sequelize, InspectionItem) {
  class Inspection extends Sequelize.Model {}
  Inspection.init({
    id: {
      primaryKey: true,
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1
    },
    status: {
      type: Sequelize.ENUM,
      values: ["pending", "new", "assigned", "recused", "waiting", "finished"]
    },
    name: Sequelize.STRING,
    description: Sequelize.STRING,
    environmentId: Sequelize.STRING
  }, { sequelize, modelName: "inspection" });
  
  Inspection.hasMany(InspectionItem);
  
  return Inspection;
};
