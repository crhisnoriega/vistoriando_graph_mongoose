module.exports = function(Sequelize, sequelize) {
  class Key extends Sequelize.Model {}
  Key.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: Sequelize.STRING,
      status: {
        type: Sequelize.ENUM,
        values: ["enable", "disable"]
      },
      other: Sequelize.STRING
    },
    { sequelize, modelName: "key" }
  );

  return Key;
};
