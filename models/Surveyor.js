module.exports = function(Sequelize, sequelize) {
  class Surveyor extends Sequelize.Model {}
  Surveyor.init({
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: Sequelize.STRING,
    cpf: Sequelize.STRING,
    rg: Sequelize.STRING,
    orgao: Sequelize.STRING,
    sexo: Sequelize.STRING,
    expe: Sequelize.STRING,
    naturalidade: Sequelize.STRING,
    nacionalidade: Sequelize.STRING,
    estadocivil: Sequelize.STRING,
    description: Sequelize.STRING,
    photo: Sequelize.STRING,
    type: Sequelize.STRING,

    cep: Sequelize.STRING,
    street: Sequelize.STRING,
    number: Sequelize.STRING,
    complemento: Sequelize.STRING,
    bairro: Sequelize.STRING,
    city: Sequelize.STRING,
    state: Sequelize.STRING,
    fone1: Sequelize.STRING,
    fone2: Sequelize.STRING,
    email: Sequelize.STRING,
  
    superAgency: Sequelize.STRING,
    accountId: Sequelize.STRING,
    
    senha: Sequelize.STRING
  }, { sequelize, modelName: "surveyor" });



  return Surveyor;
};
