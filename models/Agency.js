module.exports = function(Sequelize, sequelize) {
  class Agency extends Sequelize.Model {}
  Agency.init({
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: Sequelize.STRING,
    cnpj: Sequelize.STRING,
    socialname: Sequelize.STRING,
    description: Sequelize.STRING,
    photo: Sequelize.STRING,
    type: Sequelize.STRING,

    cep: Sequelize.STRING,
    street: Sequelize.STRING,
    number: Sequelize.STRING,
    complemento: Sequelize.STRING,
    bairro: Sequelize.STRING,
    city: Sequelize.STRING,
    state: Sequelize.STRING,
    fone1: Sequelize.STRING,
    fone2: Sequelize.STRING,
    email: Sequelize.STRING,
  
    superAgency: Sequelize.STRING,
    accountId: Sequelize.STRING
  }, { sequelize, modelName: "agency" });



  return Agency;
};
