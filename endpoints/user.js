const Joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const { User } = require("../persistence/db");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = [
  {
    method: "GET",
    path: "/user/auth",
    config: {
      handler: async function(req, res) {
        var user = await User.findOne({ where: { login: req.query.login } });

        if (user == null) {
          return res.response({ error: "usuario nao encontrado" }).code(201);
        }

        var comparePassword = await bcrypt.compare(
          req.query.password,
          user.password
        );

        if (!comparePassword) {
          return res.response({ error: "senha errada" }).code(201);
        }

        var token = jwt.sign(
          {
            data: user.login
          },
          "kiwkiwrocks",
          { expiresIn: 60 * 60 * 60 }
        );
        user.token = token;
        user.save();

        user.password = null;
        return res.response(user).code(201);
      },
      description: "Authenticate user",
      notes: "Return session token",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          login: Joi.string()
            .required()
            .description("username"),
          password: Joi.string()
            .required()
            .description("password"),
          fcmToken: Joi.string()
            .required()
            .description("FCM token")
        }
      }
    }
  },
  {
    path: "/user/{userid}",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        req.payload.password = bcrypt.hashSync(req.payload.password, 1);

        var result = await User.update(req.payload, {
          where: { id: req.params.userid }
        });

        if (result[0] == 1) {
          return res.response({ status: "OK" });
        } else {
          return res.response({ status: "error" });
        }
      },
      description: "Update user info",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          userid: Joi.string()
            .required()
            .description("user id")
        },
        payload: {
          firstName: Joi.string().description("user's firt name"),
          lastName: Joi.string().description("user's last name"),
          email: Joi.string().description("user's email"),
          password: Joi.string().description("user's password")
        }
      }
    }
  },
  {
    path: "/user",
    method: "POST",

    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        // login is phone
        req.payload.password = bcrypt.hashSync(req.payload.password, 1);
        req.payload.status = "new";

        var user = await User.findOne({ where: { login: req.payload.login } });
        if (user != null) {
          return res.response({ status: "user already register" }).code(501);
        }

        try {
          var user = await User.create(req.payload);
          return res.response(user);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register new user",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          login: Joi.string()
            .required()
            .description("user CPF"),
          phone: Joi.string().description("main phone user"),
          firstName: Joi.string()
            .required()
            .description("user's firt name"),
          lastName: Joi.string().description("user's last name"),
          email: Joi.string().description("user's email"),
          password: Joi.string().description("user's password")
        }
      }
    }
  },
  {
    path: "/user/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var users = await User.findAll({
            where: {
              firstName: {
                [Op.like]: "%" + req.query.query + "%"
              }
            },

            order: [["createdAt", "DESC"]]
          });

          return res.response(users);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query user info",
      notes: "Query user info",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  },
  {
    path: "/user/{userid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        var result = await User.destroy({
          where: { login: req.params.userid }
        });

        return res.response(result);
      },
      description: "Update user info",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          userid: Joi.string()
            .required()
            .description("user id")
        }
      }
    }
  }
];
