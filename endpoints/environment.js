const fs = require("fs");

const Joi = require("joi");
const { Environment, EnvironmentItem } = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");
const { sendFCMMessage } = require("../firebase/fcm");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");
// const redis = require("../aws/elasticache");

var itensSchema = Joi.object().keys({
  description: Joi.string()
    .required()
    .description("username"),
  type: Joi.string()
    .required()
    .description("username")
});

var userSchema = Joi.object().keys({
  userId: Joi.string()
    .required()
    .description("username")
});

module.exports = [
  {
    path: "/environments",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        if (req.payload.id == null) {
          req.payload.id = uuidv1();
        }

        var env = await Environment.create(req.payload);

        return res.response(env);
      },
      description: "Register Environments",
      notes: "Register Environments",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/environments/{environmentid}",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        var result = await Environment.update(req.payload, {
          where: { id: req.params.environmentid }
        });

        return res.response(result);
      },
      description: "Update environments status",
      notes: "Update environments status",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          environmentid: Joi.string().required()
        },
        payload: {
         
        }
      }
    }
  },
  {
    path: "/environments",
    method: "GET",
    config: {
      handler: async function(req, res) {
        console.log("get environments");

        try {
          var envs = await Environment.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(envs);
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        }
      }
    }
  },
  {
    path: "/environments/itens/{environmentid}",
    method: "GET",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var itens = await EnvironmentItem.findAll({
            where: { environmentId: req.params.environmentid }
          });

          return res.response(itens);
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          environmentid: Joi.string().required()
        }
      }
    }
  },
  {
    path: "/environments/itens",
    method: "GET",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var itens = await EnvironmentItem.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(itens);
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        }
      }
    }
  },
  {
    path: "/environments/additem/{environmentid}",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);
        console.log(JSON.stringify(req.payload));

        try {
          var environment = await Environment.findOne({
            where: { id: req.params.environmentid }
          });

          console.log(environment);

          var itens = req.payload.itens;
          var newitens = [];

          for (var index = 0; index < itens.length; index++) {
            var item = itens[index];
            if (item.id == null) {
              item.id = uuidv1();
            }
            item.environmentId = req.params.environmentid;
            var newitem = await EnvironmentItem.create(item);
          }

          return res.response({ status: "OK" });
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          environmentid: Joi.string().required()
        },
        payload: {}
      }
    }
  },
  {
    path: "/environments/createitem",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);
        console.log(JSON.stringify(req.payload));

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }
          var item = await EnvironmentItem.create(req.payload);

          return res.response(item);
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/environments/updateitem/{itemid}",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);
        console.log(req.params.itemid);

        var result = await EnvironmentItem.update(req.payload, {
          where: { id: req.params.itemid }
        });

        return res.response(result);
      },
      description: "Update environments status",
      notes: "Update environments status",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          itemid: Joi.string()
        },
        payload: {
          
        }
      }
    }
  },
  {
    path: "/environments/additem2/{environmentid}",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var environment = await Environment.findOne({
            where: { id: req.params.environmentid }
          });

          console.log(environment);

          environment.itens = req.payload.itens;

          environment.save();

          return res.response(environment);
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          environmentid: Joi.string().required()
        },
        payload: {}
      }
    }
  },

  {
    path: "/environments/{environmentid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var result = await Environment.destroy({
            where: { id: req.params.environmentid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          environmentid: Joi.string().required()
        }
      }
    }
  },
  {
    path: "/environments/itens/{itemid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var result = await EnvironmentItem.destroy({
            where: { id: req.params.itemid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Accept inspection",
      notes: "Accept inspection",
      tags: ["api", "environments"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          itemid: Joi.string().required()
        }
      }
    }
  }
];
