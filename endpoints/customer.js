const fs = require("fs");

const Joi = require("joi");
const {
  Customer,
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/customer",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var customer = await Customer.create(req.payload);

          customer.save();

          return res.response(customer);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register customer",
      notes: "customer",
      tags: ["api", "customer"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/customer/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var customers = await Customer.findAll({
            where: { customertype: req.query.query },
            order: [["createdAt", "DESC"]]
          });

          return res.response(customers);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query customers info",
      notes: "Query for customers info",
      tags: ["api", "customer"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  }
];
