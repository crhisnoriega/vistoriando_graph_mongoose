const fs = require("fs");

const Joi = require("joi");
const {
  Key,
  Service,
  Model,
  Surver,
  Customer,
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/key",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var key = await Key.create(req.payload);

          key.save();

          return res.response(key);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register key",
      notes: "key",
      tags: ["api", "key"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/key/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var keys = await Key.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(keys);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query keys info",
      notes: "Query for keys info",
      tags: ["api", "key"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .description("query")
        }
      }
    }
  }
];
