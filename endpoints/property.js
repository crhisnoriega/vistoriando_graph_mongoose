const fs = require("fs");

const Joi = require("joi");
const {
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/property",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var property = await Property.create(req.payload);

          property.save();

          return res.response(property);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register property",
      notes: "property",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/property/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var property = await Property.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(property);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query property info",
      notes: "Query for property info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  }
];
