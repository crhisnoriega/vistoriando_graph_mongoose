const fs = require("fs");

const Joi = require("joi");
const {
  Medidor,
  Key,
  Service,
  Model,
  Surver,
  Customer,
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/medidor",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var medidor = await Medidor.create(req.payload);

          medidor.save();

          return res.response(medidor);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register medidor",
      notes: "medidor",
      tags: ["api", "medidor"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/medidor/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var medidores = await Medidor.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(medidores);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query medidores info",
      notes: "Query for medidores info",
      tags: ["api", "medidor"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string().description("query")
        }
      }
    }
  },
  {
    path: "/medidor/{medidorid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        console.log(req.params.medidorid);

        try {
          var result = await Medidor.destroy({
            where: { id: req.params.medidorid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Delete Medidor",
      notes: "Medidor",
      tags: ["api", "medidor"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          surveyorid: Joi.string()
            .required()
            .description("medidor id")
        }
      }
    }
  }
];
