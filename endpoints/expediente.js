const fs = require("fs");

const Joi = require("joi");
const {
  Expediente,
  Medidor,
  Key,
  Service,
  Model,
  Surver,
  Customer,
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/expediente",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var expediente = await Expediente.create(req.payload);

          expediente.save();

          return res.response(expediente);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register medidor",
      notes: "medidor",
      tags: ["api", "medidor"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/expediente/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var expedientes = await Expediente.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(expedientes);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query expedientes info",
      notes: "Query for expedientes info",
      tags: ["api", "expediente"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string().description("query")
        }
      }
    }
  },
  {
    path: "/expediente/{expedienteid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        console.log(req.params.expedienteid);

        try {
          var result = await Expediente.destroy({
            where: { id: req.params.expedienteid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Delete Expediente",
      notes: "Expediente",
      tags: ["api", "expediente"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          expedienteid: Joi.string()
            .required()
            .description("expediente id")
        }
      }
    }
  },
  {
    path: "/language/pt_BR.js",
    method: "GET",
    config: {
      handler: async function(req, res) {
         return res.file('/home/ubuntu/environment/vistoriandoapi/langs/pt_BR.js',{confine: false});
      },
      description: "Delete Expediente",
      notes: "Expediente",
      tags: ["api", "expediente"],
      validate: {
        options: {
          allowUnknown: true
        }
       
      }
    }
  }
];
